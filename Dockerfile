FROM node:8.11-stretch

ENV NODE_ENV=production

RUN mkdir /app
WORKDIR /app

#COPY package.json package-lock.json ./

RUN npm install --production

COPY . .

RUN mkdir -p /app/uploads && chmod -R 777 /app/uploads

CMD ["npm", "run", "dev"]

EXPOSE 3000
